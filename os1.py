import os

def main():
    print('-- get current directory:')
    print(os.getcwd())
    print(f'-- get all files and fodlers from the current directory {os.getcwd()}:')
    print(os.listdir())
    os.chdir('d:/itay')
    print(f'-- get all files and fodlers from the current directory {os.getcwd()}:')
    print(os.listdir())
    #try:
#        os.mkdir('new1/new2')
#    except Exception as err:
#        print('1 -----------------------------------------')
#        print("Error creating folder")
#        print(err)
#        print('-----------------------------------------')
    try:
        os.makedirs('new1/new2') # creates 2 folders at once!
    except Exception as err:
        print('2 -----------------------------------------')
        print("Error creating folder")
        print(err)
        print('-----------------------------------------')

    import shutil
    shutil.rmtree('new1')

    os.rename('goodbye.txt', 'hello.txt')
    print(f'list of files before rename {os.getcwd()}: hello.txt ==> goodby.txt')
    print(os.listdir())

    os.rename('hello.txt','goodbye.txt')
    print(f'list of files after rename {os.getcwd()}: hello.txt ==> goodby.txt')
    print(os.listdir())

    #os.rename('goodbye.txt', 'new.txt') # cannot rename into an existing file
    print('this is all the data os.stat returned on file goodbye.txt:')
    print(os.stat('goodbye.txt'))
    mod_time = os.stat('goodbye.txt').st_mtime
    from datetime import datetime
    print(f'current time of now = {datetime.now()}')
    print(f'Last modification time of goodbye.txt: {datetime.fromtimestamp(mod_time)}') #22:56:01.123

    print('========================= walk ===========================')

    counter = 1
    #file_name = input("which file to look for?")
    for where_am_i, list_of_folders, list_of_files in os.walk(r'd:\itay'):
        # list_of_folders: 1, inner, inner2
        # list_of_files: goodbye.txt. new .txt
        print('counter',counter)
        print('where am i?',where_am_i)
        print('list of files:',list_of_files)
        print('list of folders:',list_of_folders)
        #counter += 1
        #if counter > 4:
        #    break
    # you should print in which folder the file_name resides
    # if you didnt find it- print "not found"

    # input file extension, for exmaple *.txt
    # print all files with full path which ends with txt

    # etgar: find the biggest files

#main()
def convert_bytes(size):
    if size < 1024:
        return str(size) + ' bytes'
    if size < 1024*1024:
        return str(round(size / 1024, 3)) + ' KB'
    if size < 1024 * 1024 * 1024:
        return str(round(size / (1024 * 1024), 3)) + ' MB'
    return str(round(size / (1024 * 1024 * 1024), 3)) + ' GB'
for where_am_i, list_of_folders, list_of_files in os.walk(r'd:\itay'):
    # list_of_folders: 1, inner, inner2
    # list_of_files: goodbye.txt. new .txt
    #print('counter',counter)
    #print('where am i?',where_am_i)
    #print('list of files:',list_of_files)
    #print('list of folders:',list_of_folders)
    for file_name in list_of_files:
        # examples why we need: os.path.join
        # d:\itay
        # 1.txt
        # d:\itay\1.txt
        # d:\itay\
        # 1.txt
        # d:\itay\\1.txt
        # os.chdir(where_am_i) # this is a solution but EXPENSIVE!
        file_mesudar = os.path.join(where_am_i, file_name)
        print(f'{file_name} {convert_bytes(os.path.getsize(file_mesudar))}')

        #1 -- chdir -- os.chdir(where_am_i)
        #2 -- give full path