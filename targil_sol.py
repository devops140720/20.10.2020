import os

def find_file(file_name, start_folder):
    for where_am_i, list_of_folders, list_of_files in os.walk(start_folder):
        if file_name in list_of_files:
            print(f'file {file_name} found! in {where_am_i}')
            return
    print(f'file {file_name} was not found')

def find_all_files_with_extension(extension_name, start_folder):
    counter = 0
    # if user enters txt (and not *.txt) then the next line is redundent
    ext_postfix = extension_name.split('.')[1] # *.txt ==> ['*','txt'] ==> 'txt'
    for where_am_i, list_of_folders, list_of_files in os.walk(start_folder):
        # list : 1.txt 2.png 3.jpg
        for file_name in list_of_files:
            # 1.txt
            file_ext = file_name.split('.')[1] # 1.txt ==> ['1','txt'] ==> 'txt'
            if ext_postfix == file_ext:
                counter += 1
                print(f'{where_am_i}\{file_name}')
    if counter == 0:
        print(f'no items match your search, {extension_name}')

def convert_bytes(size):
    if size < 1024:
        return str(size) + ' bytes'
    if size < 1024 * 1024:
        return str(round(size / 1024, 3)) + ' KB'
    if size < 1024 * 1024 * 1024:
        return str(round(size / (1024 * 1024), 3)) + ' MB'
    return str(round(size / (1024 * 1024 * 1024), 3)) + ' GB'

def find_biggest_file_size(start_folder):
    max_size = 0
    file_max_name = None
    for where_am_i, list_of_folders, list_of_files in os.walk(start_folder):
        # list_of_folders: 1, inner, inner2
        # list_of_files: goodbye.txt. new .txt
        # print('counter',counter)
        # print('where am i?',where_am_i)
        # print('list of files:',list_of_files)
        # print('list of folders:',list_of_folders)
        for file_name in list_of_files:
            # examples why we need: os.path.join
            # d:\itay
            # 1.txt
            # d:\itay\1.txt
            # d:\itay\
            # 1.txt
            # d:\itay\\1.txt
            # os.chdir(where_am_i) # this is a solution but EXPENSIVE!
            file_mesudar = os.path.join(where_am_i, file_name)
            size = os.path.getsize(file_mesudar)
            if size > max_size:
                max_size = size
                file_max_name = file_mesudar
    return file_max_name, max_size

def targil1():
    fname = input("Which file are you looking for? ")
    dname = input("From which directory to start? ")
    find_file(fname, dname)

def targil2():
    fname = input("Please type the extension (i.e. *.txt) ")
    dname = input("From which directory to start? ")
    find_all_files_with_extension(fname, dname)

def targil3():
    dname = input("From which directory to start? ")
    file_name, max_size = find_biggest_file_size(dname)
    print(f'{file_name} {convert_bytes(max_size)}')

targil3()